const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Name is required"]
	},

	description: {
		type: String,
		required: [true, "Description is required"]
	},

	price: {
		type: Number,
		required: [true, "Price is required"]
	},

	image: {
		type: String,
		required: true
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date() // to make sure that the product will be recorded based on the current date.
		}




	});


module.exports = mongoose.model("Product", productSchema);