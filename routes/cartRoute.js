const express = require('express');
const router = express.Router();
const cartController = require('../controllers/cartController');
const auth = require('../auth');


router.post('/add/:productId', auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		name: req.body.name,
		quantity: req.body.quantity
	}

	if(data.isAdmin == false){
	cartController.addCart(data, req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("Only users can create order")
	}


})

router.get('/', auth.verify, (req, res)=> {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	cartController.getCart(data).then(resultFromController => res. send(resultFromController))

})

router.get('/all', auth.verify, (req, res)=> {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
	cartController.getAll().then(resultFromController => res. send(resultFromController))
	} else {
		res.send("Restricted")
	}

})







module.exports = router;