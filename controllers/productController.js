const Product = require('../model/Product');
const User = require('../model/Users');
const auth = require('../auth');

module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		image: reqBody.image

	})

	return newProduct.save().then((product, error) => {

		if(error) {

			return false

		} else {

			return true
			

		}

	})

}

// Retrieve all products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};


// Retrieve ACTIVE PRODUCTS
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};


// Retrieve specific product
module.exports.getProducts = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};


module.exports.updateProduct = (reqParams, reqBody) => {
	
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		image: reqBody.image
	};

	// Syntax: findByIdAndUpdate(document ID, udpatesToBeApplied)

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			} else {
				return true;
			};

	});

};


module.exports.archiveProduct = (productId) => {

	return Product.findById(productId).then((result, error)=> {

		if(error){
			console.log(error)
			return false
		} else {

			result.isActive = false

			return result.save().then((res, err) => {
				if(err){
					console.log(err)
				} else {
					return true
				}
			})

		}

	})

}

module.exports.unArchiveProduct = (productId) => {

	return Product.findById(productId).then((result, error)=> {

		if(error){
			console.log(error)
			return false
		} else {

			result.isActive = true

			return result.save().then((res, err) => {
				if(err){
					console.log(err)
				} else {
					return true
				}
			})

		}

	})

}