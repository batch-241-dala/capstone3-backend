const Product = require('../model/Product');
const User = require('../model/Users');
const Cart = require('../model/Carts');
const auth = require('../auth');

module.exports.addCart = (data, reqParams) => {

	return Product.findById(reqParams.productId).then(product => {
				let newCart = new Cart({

					userId: data.userId,
					productId: reqParams.productId,
					name: data.name,
					quantity: data.quantity,
					subTotal: product.price * data.quantity
					

				})
				return newCart.save().then((cart, error) => {
				 	
				 	if(error) {
				 		return false
				 	} else {
				 		return true
				 	}
				
			})
	

	})
}

module.exports.getCart = (data) => {

	return User.findById(data.userId).then(user => {
		return Cart.find({userId: data.userId}).then(cart => {
			
			return cart

		})
	})
	

}

module.exports.getAll = (data) => {

	return Cart.find({}).then((cart, error) => {

		if(error){
			return false
		} else {
			return cart
		}

	})
	

}