const mongoose = require('mongoose');

const cartSchema = new mongoose.Schema({

	 
		
			userId: {
				type: String,
				required: true
			},
			productId: {
				type: String,
				required: true
			},

			name: {
				type: String,
				required: true
			},

			subTotal: {
				type: Number,
				required: true
			},

			quantity: {
				type: Number,
				required: true
			},

			purchasedOn: {
				type: Date,
				default: new Date() // to make sure that the product will be recorded based on the current date.
				}
		


});


module.exports = mongoose.model("Cart", cartSchema);