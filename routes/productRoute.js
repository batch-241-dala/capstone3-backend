const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');


// Route for creating a product
router.post("/create", auth.verify, (req, res) => {

	const  userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == true) {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}
	
})

// Route for retrieving all the product
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});


// Retrieve all the ACTIVE products
router.get("/active", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// Retrieve specific product
router.get("/:productId", (req, res) => {
	productController.getProducts(req.params).then(resultFromController => res.send(resultFromController));
});



router.put("/update/:productId", auth.verify, (req, res) => {

	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));

})

router.patch('/archive/:id', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		productController.archiveProduct(req.params.id).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("User not authorized to update product")
	}
})

router.patch('/unArchive/:id', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		productController.unArchiveProduct(req.params.id).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("User not authorized to update product")
	}
})








module.exports = router;