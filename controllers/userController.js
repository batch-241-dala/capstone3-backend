const User = require("../model/Users");
const Product = require("../model/Product")
const bcryptjs = require('bcryptjs');
const auth = require('../auth');


module.exports.checkEmailExists = (reqBody) => {

	return User.find({email:reqBody.email}).then( result => {

		if (result.length > 0) {
			return true
		// no duplicate email found
		} else {
			return false
		}

	});

};


module.exports.registerUser = (reqBody) => {

	let newUser = new User ({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcryptjs.hashSync(reqBody.password, 10), // bcrypt.hashSync(<dataToBeHash>,<saltRound>)
		mobileNo: reqBody.mobileNo

	})

	return newUser.save().then((user, error) => {

		if (error) {
			return false
		} else {
			return user
		}

	})

};

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		
		// console.log(result)
		if(result == null) {
			
			return false
		
		} else {

			// compareSync(dataFromRequestBody, encryptedDataFromDatabase)
			const isPasswordCorrect = bcryptjs.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				
				return { access: auth.createAccessToken(result) }
			
			} else {
				
				return false
			
			};
		};
	});

};

module.exports.getProfile = (reqBody) => {

	return User.findById(reqBody.id).then(result => {

		if(result == null) {
			
			return false
		
		} else {

			result.password = "";
			return result;

		}

	})

}

module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({productId: data.productId});
		return user.save().then((user, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {
			product.enrollees.push({userId: data.userId});
			return product.save().then((product, error) => {
				if(error){
					return false
				} else {
					return true
				}
			})
	})

	if(isUserUpdated && isProductUpdated){
		return true
	} else {
		return false
	}

}

// Retreive user details (For Frontend)
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		// console.log(data.userId);
		// console.log(result);
		
		if (result == null) {
			return false
		} else {
			result.password = "*****"

			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};

module.exports.changePassword = (userData, reqBody) => {

	let updatedPassword = {

		password: bcryptjs.hashSync(reqBody.password, 10)

	}

	return User.findByIdAndUpdate(userData.id, updatedPassword).then((result, error) => {

		console.log(result)
		if(error) {
			return false
		} else {

			return true

		}

	})

}


